# acc_private_key is a file path
# private_key is a file path
# domains_list is an array of domains to sign
# Gen new key openssl genrsa -out le_acc_private.key 2048
#
# TODO: auto generate acc private key and domain private key if not presented in config file
class GetLetsEncrypt
  def initialize(acc_private_key, private_key, domains_list = [], email = nil, kid = "")
    # LetsEncrypt account key
    @acc_private_key = OpenSSL::PKey::RSA.new(File.read(acc_private_key))
    # Domain private key
    @private_key = OpenSSL::PKey::RSA.new(File.read(private_key))
    @domains_list = domains_list
    @email = email
    @kid = kid.empty? ? nil : kid
  end

  def prod_client
    Acme::Client.new(private_key: @acc_private_key, directory: 'https://acme-v02.api.letsencrypt.org/directory', kid: @kid)
  end

  def staging_client
    Acme::Client.new(private_key: @acc_private_key, directory: 'https://acme-staging-v02.api.letsencrypt.org/directory', kid: @kid)
  end

  # register LE account
  def register(client)
    account = client.new_account(contact: "mailto:#{@email}", terms_of_service_agreed: true)
    puts "add it to the config file => KID: #{account.kid}"
  end

  def authorization(client)
    order = client.new_order(identifiers: @domains_list)
    authorization = order.authorizations
    # TODO: write kid in file or read if exists
    puts "add kid to the config file kid: #{client.kid}" if @kid.nil?

    authorization.each do |i|
      challenge = i.dns
      domain_name = challenge.record_name + '.' + i.domain
      create_cf_dns_record(domain_name, challenge.record_type, challenge.record_content)
    end

    authorization.each do |i|
      challenge = i.dns
      challenge.request_validation
      puts '>' + challenge.record_name + '.' + i.domain
      while challenge.status == 'pending'
        puts challenge.status
        sleep(2)
        challenge.reload
      end
      puts challenge.status
    end
    order
  end

  # domains_list has to be an array
  def sign_le_certificate(order)
    csr = Acme::Client::CertificateRequest.new(private_key: @private_key, names: @domains_list)
    order.finalize(csr: csr)
    sleep(1) while order.status == 'processing'
    certificate = order.certificate # => PEM-formatted certificate
    File.write("#{CONF['dir_to_save_pem']}/#{@domains_list[0]}.pem", certificate) if CONF['save_pem']
    certificate.to_json
  end
end
