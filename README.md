COMBINE
====================

**_Short description_**

Function of the project is to generate ssl certificate, sign it with help of Let's Encrypt (dns-01)
and upload it to CloudFlare (CF) and GCP.

**You may need to install 'zlibc libxml2-dev libssl-dev'**.
For now only CloudFlare DNS is supported.
~~For now certificate can be signed only for one domain name group `Ex: domain.com, *.domain.com, *.sub.domain.com`.~~
For now you can't skip any step like upload ssl to CF or GCP

You need to create file .secret.rb with 

>`EMAIL = 'CF email'`  
>`KEY = 'CF API token'`

Params available:
>  --conf conf.d/exemple.json

**_Options are in_** `config.json`
 
- "acc_private_key":"ssl key for Let’s Encrypt account",
- "private_key":"your domain private key",
- "email":"email for Let’s Encrypt notifications",
- "domains":"domains to sign, must be white space separated",
- "kid":"Let’s Encrypt account id, will be displayed if account just registered",
- "staging":"can be true or false"",
- "google_project":"google project name",
- "google_json_key_location":"location of service account json file",
- "google_lb":"google targetHttpsProxie name",
- "save_pem":"can be true or false",
- "dir_to_save_pem":"dir where you want to save optained ssl crt",
- >>> Cisco ASA only, can't be enabled with CF and google crt upload
- "asa_crt_upload":"can be true or false",
- "asa_hostname":"asa-mgmt hostname",
- "asa_username":"cli-access-user",
- "asa_password":"password"

**_Plans:_**
- Add PowerDNS support.
- Add DNS provider option.
- ~~Add multi domain~~.
- Add upload option, like skip ~~GCP~~ or CF.
- ~~Cisco ASA support~~

**GEMs used:**
- 'fog-google'
- 'json'
- 'acme-client'
- 'openssl'
- 'dnsruby'
