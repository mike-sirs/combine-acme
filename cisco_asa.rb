# Convert pem, upload and pin certificate to Cisco ASA

class CiscoASA
  def initialize( private_key, public_key, username, password, asa_hostname, domain_name)
    @private_key = private_key
    @public_key = public_key
    @username = username
    @password = password
    @asa_hostname = asa_hostname
    @domain_name = domain_name
  end

  CRT_NAME = "LE_#{@domain_name}_#{Time.now.to_i.to_s}"

  def convert_to_p12
  # Create p12 crt
    cert = OpenSSL::X509::Certificate.new(eval(@public_key))
    pkey = OpenSSL::PKey::RSA.new(File.read(@private_key))
    OpenSSL::PKCS12.create('BVkald88sa', 'Imported Certificate', pkey, cert)
  end

  def upload_crt(p12)
    uri = URI.parse("https://#{@asa_hostname}/api/certificate/identity")
    crt = ['-----BEGIN PKCS12-----'] + Base64.encode64(p12.to_der).split("\n") + ['-----END PKCS12-----']
    payload = {
        "certPass": 'BVkald88sa',
        "kind": 'object#IdentityCertificate',
        "certText": crt,
        "name": CRT_NAME
    }

    creds = Base64.encode64(@username + ':' + @password)
    req = Net::HTTP::Post.new(uri)
    req['Content-Type'] = 'application/json'
    req['Authorization'] = 'Basic' + ' ' + creds.chomp
    req.body = payload.to_json

    Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == 'https') do |http|
      http.request(req) do |res|
        puts res.body
      end
    end
  end

  def pin_crt
    uri = URI.parse("https://#{@asa_hostname}/api/cli")
    command0 = 'ssl trust-point ' + CRT_NAME + ' domain asa.apalon.com'
    command1 = 'ssl trust-point ' + CRT_NAME + ' outside'
    command2 = 'ssl trust-point ' + CRT_NAME + ' inside'
    command3 = 'crypto ikev2 remote-access trustpoint ' + CRT_NAME
    payload = {'commands': [command0, command1, command2, command3, 'write']}

    creds = Base64.encode64(@username + ':' + @password)
    req = Net::HTTP::Post.new(uri)
    req['Content-Type'] = 'application/json'
    req['Authorization'] = 'Basic' + ' ' + creds.chomp
    req.body = payload.to_json

    Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == 'https') do |http|
      http.request(req) do |res|
        puts res.body
      end
    end
  end
end